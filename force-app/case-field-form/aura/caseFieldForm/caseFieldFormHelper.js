/**
 * Created by chandler.anderson on 2019-03-22.
 */
({
    getData: function (component, helper) {
        var action = component.get("c.getData");
        action.setParams({
                "fieldSetName": component.get("v.fieldSetName")
            }
        );
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log(state);

            if (component.isValid() && state === "SUCCESS") {
                var responseObject = response.getReturnValue();
                if (responseObject.isSuccess) {
                    component.set("v.caseFields", responseObject.caseFields);
                } else {
                    helper.throwError(component, responseObject.message);
                }
            } else {
                helper.showSpinner(component, false);
                if (state === "INCOMPLETE")
                    console.log("Incomplete");
                else if (state === "ERROR")
                    helper.throwError(component, response.getError());
                else if (!component.isValid())
                    helper.throwError(component, "Something went wrong. Please refresh the page.");
            }
            helper.showSpinner(component, false);
        });
        helper.showSpinner(component, true);
        $A.enqueueAction(action);
    },
    throwError: function (component, error) {
        var toastEvent = $A.get("e.force:showToast");
        error = ( error == null ? "Unknown Error" : (Array.isArray(error) ? error[0].message : error));
        console.log("Error message: " + error);
        if (toastEvent) {
            toastEvent.setParams({
                "title": "Error!",
                "message": error,
                "type": "error"
            });
            toastEvent.fire();
        }
    },
    showSpinner: function (component, showSpinner) {
        if (showSpinner) {
            $A.util.removeClass(component.find('spinner'),'slds-hide');
        } else {
            $A.util.addClass(component.find('spinner'),'slds-hide');
        }
    }
})