/**
 * Created by chandler.anderson on 2019-03-22.
 */
({
    doInit: function (component, event, helper) {
        console.log('init');
        helper.getData(component, helper);
    },

    handleLoad: function (component, event, helper) {
        console.log('recordEditForm loaded');
        //"Zero-out" empty input field values so defaults don't show.
        var inputFields = component.find("input-field");
        if (inputFields) {
            inputFields.forEach(function (inputField) {
                var inputValue = inputField.get("v.value");
                if (!inputValue) inputField.set("v.value", null);
            });
        }
        //Show field area
        $A.util.removeClass(component.find("field-area"),"slds-hide");
        //Hide spinner
        helper.showSpinner(component, false);
    },

    handleSubmit: function (component, event, helper) {
        console.log('recordEditForm submit');
        event.preventDefault(); //Stop the form from using standard submit function
        var eventFields = event.getParam("fields"); //Store the fields from the event

        var caseFields = component.get("v.caseFields");
        caseFields.forEach(function (field) {
            //Individual values can be accessed
            console.log(field.apiName + ' = ' + field.value);

            //Ex: manipulate them and save in eventFields
            //  <<something that manipulates the field value>>
            //  eventFields[apiName] = value;
        });

        //Do a thing with them

        //Ex. continue submission with new field values from above
        //  component.find("case-form").submit(eventFields);
    },

    handleSuccess: function (component, event, helper) {
        console.log('recordEditForm success');
        var payload = event.getParams().response;
        helper.showSpinner(component,false);
    },

    handleError: function (component, event, helper) {
        console.log('recordEditForm ERROR');
    }
})