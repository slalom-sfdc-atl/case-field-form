/**
 * Created by chandler.anderson on 2019-03-22.
 */

public with sharing class CaseFieldFormController {

    public class ResponseModel {
        @AuraEnabled public Boolean isSuccess;
        @AuraEnabled public String message;
        @AuraEnabled public List<FieldSetMemberForAura> caseFields;

        public ResponseModel() {
            this.isSuccess = true;
        }

        public ResponseModel(String message, Boolean isSuccess) {
            this.message = message;
            this.isSuccess = isSuccess;
        }

        public ResponseModel(Exception e) {
            this.message = 'Error: ' + e.getStackTraceString() + ': ' + e.getMessage();
            this.isSuccess = false;
        }
    }

    /**
     * This class allows field sets to be pulled into a Lightning Component.
     */
    public class FieldSetMemberForAura {
        @AuraEnabled public String apiName;
        @AuraEnabled public String label;
        @AuraEnabled public Boolean isRequired;
        @AuraEnabled public Boolean isDbRequired;
        @AuraEnabled public String type;

        @AuraEnabled public String value;

        public FieldSetMemberForAura(Schema.FieldSetMember fieldSetMember) {
            this.apiName = fieldSetMember.getFieldPath(); //api name
            this.label = fieldSetMember.getLabel();
            this.isRequired = fieldSetMember.getRequired();
            this.isDbRequired = fieldSetMember.getDbRequired();
            this.type = String.valueOf(fieldSetMember.getType());   //type - STRING,PICKLIST
        }
    }

    @AuraEnabled
    public static ResponseModel getData(String fieldSetName) {
        try {
            if (fieldSetName != null) {
                ResponseModel response = new ResponseModel();
                response.caseFields = getFieldSetMembers('Case', fieldSetName);
                return response;
            } else {
                return new ResponseModel('No field set name specified.', false);
            }
        } catch (Exception e) {
            return new ResponseModel(e);
        }
    }


    /**
     * Get a field sets for an object by its name, and returns them in a form that can be used by Lightning Components.
     *
     * @param objectTypeName The name of an SObject (like Case).
     * @param fieldSetName The name of a field set.
     *
     * @return A list of FieldSetMemberForAura objects.
     */
    public static List<FieldSetMemberForAura> getFieldSetMembers(String objectTypeName, String fieldSetName) {
        DescribeSObjectResult[] describes = Schema.describeSObjects(new String[]{
                objectTypeName
        });

        if (describes != null && describes.size() > 0) {
            // There should only be the one match for the one object type name
            Schema.FieldSet fs = describes[0].fieldSets.getMap().get(fieldSetName);

            if (fs != null) {
                List<FieldSetMemberForAura> fields = new List<FieldSetMemberForAura>();
                for (Schema.FieldSetMember fsm : fs.fields) {
                    fields.add(new FieldSetMemberForAura(fsm));
                }
                return fields;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

}